@extends('layout')

@section('content')
    @include('errors')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$task->name}}</h3>
                <p>
                    {{$task->comment}}
                </p>
            </div>
        </div>

    </div>
@endsection