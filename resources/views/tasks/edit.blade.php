@extends('layout')

@section('content')
    @include('errors')
    <div class="container">
        <h3>Edit Feedback # - {{$task->id}}</h3>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => ['tasks.update', $task->id], 'method' => 'PUT']) !!}
                <div class="form-group">
                    <h3>Name</h3>
                    <input type="text" class="form-control" name="name" value="{{$task->name}}">
                    <br>
                    <h3>E-Mail</h3>
                    <input type="text" class="form-control" name="email" value="{{$task->email}}">
                    <br>
                    <h3>Comment</h3>
                    <textarea name="comment" id="" cols="30" rows="10" class="form-control" >{{$task->comment}}</textarea>
                    <br>
                    <button class="btn btn-success">Submit</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection