
@extends('layout')

@section('content')
    <div class="container">
            <h3>Feedbacks</h3>
            <a href="{{ route('tasks.create') }}" class="btn btn-success">Give feedback</a>
            <div class="row">

            <div class="col-md-10 col-md-offset-1">
                <table class="table">

                    <tr>
                    @foreach($tasks as $task)

                            <tr>Name - {{$task->name}} <a href="{{route('tasks.show', $task->id)}}">
                                    <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                                </a>

                                <a href="{{route('tasks.edit', $task->id)}}">
                                    <i class="glyphicon glyphicon-edit" aria-hidden="true"></i>
                                </a>

                                {!! Form::open(['method'=>'DELETE',
                                  'route'=>['tasks.destroy', $task->id]]) !!}
                                <button onclick="return confirm('Are you sure?')">
                                    <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                {!! Form::close() !!} </tr><br>

                            <tr>E-Mail - {{$task->email}}</tr><br>
                            <tr>Comment: {{$task->comment}} </tr><br>
                            <hr>



                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection